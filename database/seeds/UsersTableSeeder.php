<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            [
                [
                'name'=> 'Roni',
                'email'=>'a@a.com',
                'password' =>  Hash::make('12345678'),
                'created_at' => date('y-m-d G:i:s'),
                'role' => 'admin',
                ],
            ]
    );
    }
}

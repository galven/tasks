@yield('content')
@extends('layouts.app')
@section('content')
<h1> Edit your Task list </h1>

<form method = 'post' action = "{{action('TaskController@update', $task ->id)}}">
@csrf
@method('PUT')

<div class="form-group">
<label for = "title">Update the name of the task</label>
<input type = "text" class = "form-control" name= "title" value = "{{$task-> title}}">
</div>

<div class = "form-group">
    <input type="submit" class="form-control" name="submit" value= "Save">
</div>
</form>
@endsection
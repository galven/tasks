<!DOCTYPE html>
<html>
@yield('content')
@extends('layouts.app')
@section('content')

<h1> Create a new Task </h1>

<form method = 'post' action = "{{action('TaskController@store')}}">
{{csrf_field()}}

<div class="form-group">
<label for = "title">What is the task you would like to Add? </label>
<input type = "text" class = "form-control" name= "title">
</div>

<div class = "form-group">
    <input type="submit" class="form-control" name="submit" value= "Save">
</div>
</form>
@endsection
</html>